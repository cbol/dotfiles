import XMonad
import XMonad.Util.EZConfig(additionalKeysP)
--import XMonad.Layout.EqualSpacing
import XMonad.Util.Run(spawnPipe)
import XMonad.Hooks.ManageDocks

main = xmonad $ defaultConfig
    --{ layoutHook = equalSpacing 36 6 0 1 $ layoutHook defaultConfig
    { modMask = mod4Mask -- Use Super instead of Alt
    , layoutHook = avoidStruts $ Tall 1 (3/100) (1/2)
    , borderWidth = 3
    , normalBorderColor = "#c0c5ce"
    , focusedBorderColor = "#b48ead"
    , terminal = "stterm -e tmux"
    } `additionalKeysP` [ --("M-d", sendMessage $ LessSpacing)
                        --, ("M-i", sendMessage $ MoreSpacing)
                        ("M-p", spawn "dmenu_run -l 10 -fn '-xos4-terminus-*-*-*-*-12-*-*-*-*-*-*-*' -nb '#2b303b' -nf '#c0c5ce' -sb '#4f5b66'") ]
