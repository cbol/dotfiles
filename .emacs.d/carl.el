
(setq-default buffer-file-coding-system 'utf-8-unix)
(fset 'yes-or-no-p 'y-or-n-p)
(setq inhibit-startup-message t)
(server-start)

(setq user-full-name "Carl Bolduc"
      user-mail-address "carlbolduc@gmail.com")

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(eval-when-compile
  (require 'use-package))
(require 'diminish)                ;; if you use :diminish
(require 'bind-key)                ;; if you use any :bind variant

(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

(use-package zenburn-theme
  :ensure t
  :config
;;  (load-theme 'zenburn t)
(load-theme 'darkokai t)
)
(tooltip-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(delete-selection-mode 1)

(setq ispell-program-name "aspell")

(use-package counsel
  :ensure t
  :config
  (ivy-mode 1)
  :bind ("M-x" . counsel-M-x)
  :bind ("C-x m" . counsel-M-x)
  :bind ("C-x C-f" . counsel-find-file)
;;  :bind ("C-c k") . counsel-ag)
)

(use-package ivy
  :ensure t
  :bind ("C-s" . swiper)
  :bind ("C-c g" . counsel-git)
  :bind ("C-c j" . counsel-git-grep)
  :bind ("C-c g" . counsel-git)
  :bind ("C-c s" . counsel-ag)
)

;; (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy)))
;;(setq ivy-use-virtual-buffers t)
;;(global-set-key (kbd "C-c C-r") 'ivy-resume)
;;(global-set-key (kbd "<f6>") 'ivy-resume)
;;(global-set-key (kbd "<f1> f") 'counsel-describe-function)
;;(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
;;(global-set-key (kbd "<f1> l") 'counsel-load-library)
;;(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
;;(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
;;(global-set-key (kbd "C-c g") 'counsel-git)
;;(global-set-key (kbd "C-c j") 'counsel-git-grep)
;;(global-set-key (kbd "C-x l") 'counsel-locate)
;;(global-set-key (kbd "C-x p") 'find-file-in-project)

(use-package org
  :ensure t
  :pin melpa
  :bind ("C-c l" . org-store-link)
  :bind ("C-c a" . org-agenda)
  :bind ("C-c c" . org-capture)
  :bind ("C-c b" . org-switchb)
)
(setq org-capture-templates
      '(("n" "Note" entry (file+headline "/sshx:cb:Dropbox/org/inbox.org" "Notes")
             "* %?\nEntered on %U")
        ("N" "Note" entry (file+headline "~/Dropbox/org/inbox.org" "Notes")
             "* %?\nEntered on %U")
        ("t" "Todo" entry (file+headline "~/Dropbox/org/inbox.org" "Tasks")
             "* TODO %?\n  %i\n  %a\nEntered on %U")
        ("j" "Journal" entry (file+datetree "~/Dropbox/org/journal.org")
             "* %U - %?")))

(org-babel-do-load-languages
      'org-babel-load-languages
      '((emacs-lisp . nil)
        (sh . t)))

(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))

(use-package company
  :ensure t
  :pin melpa
  :config
  (add-hook 'after-init-hook 'global-company-mode)
)
(use-package company-tern
  :ensure t
  :config
  (add-to-list 'company-backends 'company-tern)
)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

(use-package magit
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-magit-file-mode)
)

(use-package tern
  :ensure t
)
(use-package js2-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist `(,(rx ".js" string-end) . js2-mode))
  (add-hook 'js2-mode-hook (lambda () (tern-mode t)))
  (add-hook 'js2-mode-hook (lambda () (set (make-local-variable 'company-backends) '(company-tern))))
)

(use-package fireplace
  :ensure t
)
