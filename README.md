# dotfiles

## post-cloning steps

- `ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf`
- `ln -s ~/dotfiles/.vim ~/.vim`
- `ln -s ~/dotfiles/.emacs.d ~/.emacs.d`
